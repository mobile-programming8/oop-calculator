import 'dart:io';

bool isNotFinish = true;
void main() {
  while (isNotFinish) {
    showMenu();
    String? input = receiveOperator();
    String operator;
    if (input == null) {
      isNotFinish = false;
      break;
    }else{
      operator = input;
    }
    selectMenu(operator);
  }
  print("End");
}

void showMenu() {
  print("Select an operator: +, -, *, or /");
}

String? receiveOperator() {
  String? input = stdin.readLineSync();
  return input;
}

void selectMenu(String operator) {
  switch (operator) {
    case "+":
      plus();
      break;
    case "-":
      minus();
      break;
    case "*":
      multiply();
      break;
    case "/":
      divide();
      break;
    default:
      isNotFinish = false;
  }
}

void divide() {
  print("Enter first number");
  var a = stdin.readLineSync();
  print("Enter second number");
  var b = stdin.readLineSync();
  var ans = double.parse(a!) / double.parse(b!);
  print("$a / $b = $ans");
}

void multiply() {
  print("Enter first number");
  var a = stdin.readLineSync();
  print("Enter second number");
  var b = stdin.readLineSync();
  var ans = double.parse(a!) * double.parse(b!);
  print("$a * $b = $ans");
}

void minus() {
  print("Enter first number");
  var a = stdin.readLineSync();
  print("Enter second number");
  var b = stdin.readLineSync();
  var ans = double.parse(a!) - double.parse(b!);
  print("$a - $b = $ans");
}

void plus() {
  print("Enter first number");
  var a = stdin.readLineSync();
  print("Enter second number");
  var b = stdin.readLineSync();
  var ans = double.parse(a!) + double.parse(b!);
  print("$a + $b = $ans");
}